#!/usr/bin/env python
# coding: utf-8


import requests

def send_notify(token, msg, filepath=None, stickerPackageId=None, stickerId=None):
    payload = {'message': msg}
    headers = {
        "Authorization": "Bearer " + token
     }
    if stickerPackageId and stickerId:
        payload['stickerPackageId'] = stickerPackageId
        payload['stickerId'] = stickerId
    
    if filepath:
        attachment = {'imageFile': open(filepath, 'rb')}
        print(attachment)
        r = requests.post("https://notify-api.line.me/api/notify", headers=headers, params=payload, files=attachment)
    else:
        r = requests.post("https://notify-api.line.me/api/notify", headers=headers, params=payload)
    return r.status_code, r.text

# Test line notify
token = "5J8q3AcjlqPVsn6BHiKeCzE7WQiqcAkgdaGTdmlY8kP"
send_notify(token=token, msg='小強你不能死阿', filepath='/mnt/c/Users/ace12/Downloads/donotdie.jpg')

